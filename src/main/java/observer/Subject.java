package observer;

/**
 * A Subject is an instance that can be monitored by adding an
 * Observer to it. When the Subject updates it notifies its
 * Observers.
 *
 * @author Max Jonsson
 * @version 2020-01-20
 */
public interface Subject {
	/**
	 * Adds an Observer to monitor the instance's changes.
	 * @param observer The observing instance
	 */
	public void addObserver(Observer observer);
	/**
	 * Notify observers that changes has been made.
	 */
	public void notifyObservers();
}
