package observer;

/**
 * Functional interface used to subscribe to a class implementing
 * the Subject interface.
 *
 * @author Max Jonsson
 * @version 2020-01-20
 */
@FunctionalInterface
public interface Observer {
	/**
	 * Method called when the Subject has updated its state.
	 */
	public void update();
}
