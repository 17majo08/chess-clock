package controller;

import chessclock.ChessClock;
import chessclock.ChessClock.Player;
import helpers.TimeFormatter;
import observer.Observer;

/**
 * The controller instance controls the logic for the action events sent
 * from the GUI and performs the appropriate actions towards the domain
 * instance ChessClock.
 * 
 * @author Max Jonsson
 * @version 2020-01-27
 */
public class Controller {
    private final ChessClock clock;
	
	public Controller() {
		this.clock = new ChessClock();
	}
	
	public void setTime(int h, int m, int s) {
		clock.setInitialTime(TimeFormatter.timeToSecs(h, m, s));
	}
	
	public void resetTimers() {
		clock.resetTimers();
	}
	
	private void swapTimer(Player player) {
		if (clock.isStarted())
			clock.swapTimer();
		else
			clock.startTimer(player);
	}
	
	public void startBlackTimer() {
		swapTimer(Player.BLACK);
	}

	public void startWhiteTimer() {
		swapTimer(Player.WHITE);
	}
	
	private String digitalizeTime(int secs) {
		return TimeFormatter.secsToTimeString(secs);
	}
	
	public String getBlackTimeString() {
		return digitalizeTime(clock.getBlackTime());
	}
	
	public String getWhiteTimeString() {
		return digitalizeTime(clock.getWhiteTime());
	}
	
	private boolean isTimeUp() {
		return clock.isTimeUp();
	}
	
	public boolean isBlackStartable() {
		return isTimeUp() ? false : !clock.isBlackRunning();
	}
	
	public boolean isWhiteStartable() {
		return isTimeUp() ? false : !clock.isWhiteRunning();
	}
	
	public void addObserver(Observer observer) {
		clock.addObserver(observer);
	}
	
}
