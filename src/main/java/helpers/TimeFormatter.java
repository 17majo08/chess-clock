package helpers;

/**
 * The TimeFormatter provides public static methods to make
 * different time related transformations.
 * 
 * @author Max Jonsson
 * @version 2020-01-21
 */
public abstract class TimeFormatter {
	public static final int SECS_PER_HOUR = 3600;
	public static final int SECS_PER_MINUTE = 60;
	
	/**
	 * Converts hours, minutes and seconds combined to a number
	 * of seconds.
	 * @param hh Number of hours
	 * @param mm Number of minutes
	 * @param ss Number of seconds
	 * @return Total number of seconds
	 */
	public static int timeToSecs(int hh, int mm, int ss) {
		return hh * SECS_PER_HOUR + mm * SECS_PER_MINUTE + ss;
	}
	
	/**
	 * Converts a number of seconds into a digital time format in
	 * total hours, minutes and seconds.
	 * @param seconds The number of seconds
	 * @return The digital time format represented as a String on the
	 * form "hh:mm:ss"
	 */
    public static String secsToTimeString(int seconds) {
		if (seconds < 0)
			throw new IllegalArgumentException("Negative numbers not allowed");
		
		int hh = seconds / SECS_PER_HOUR;
		int mm = (seconds % SECS_PER_HOUR) / SECS_PER_MINUTE;
		int ss = (seconds % SECS_PER_HOUR) % SECS_PER_MINUTE;
		
		try {
			return String.join(":", digitalize(hh), digitalize(mm), digitalize(ss));
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("Number too large to display");
		}
	}
	
	private static String digitalize(int num) {
		if (num < 0 || num > 99)
			throw new IllegalArgumentException(String.format("Number %d too large. Only positive one or two digit numbers allowed", num));
		
		return String.format(num < 10 ? "0%d" : "%d", num);
	}
	
}
