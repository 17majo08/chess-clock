package gui;

import controller.Controller;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;

/**
 * GUI for the ChessClock implementation using javax.swing.
 * 
 * @author Max Jonsson
 * @version 2020-01-23
 */
public class ChessClockGUI {
	private final Controller controller;
	private JFrame window;
	private JLabel blackTimeLabel, whiteTimeLabel;
	private JLabel hourLabel, minLabel, secLabel;
	private JButton timeBtn, resetBtn, blackBtn, whiteBtn;
	private JSpinner hourSpinner, minuteSpinner, secondSpinner;
	
	public ChessClockGUI() {
		initComponents();
		
		this.controller = new Controller();
		
		controller.addObserver(() -> updateGUI());
		window.setVisible(true);
	}
	
	private void setDefaultFont() {
		Font font = new Font("Sans", Font.PLAIN, 20);
		UIManager.put("Label.font", font);
		UIManager.put("Button.font", font);
		UIManager.put("Spinner.font", font);
	}

	private void initComponents() {
		setDefaultFont();
		
		this.window = new JFrame("Chess Clock");
		window.setLayout(new GridBagLayout());
		window.setSize(350, 350);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.hourLabel = new JLabel("Hours:");
		this.minLabel = new JLabel("Mins:");
		this.secLabel = new JLabel("Secs:");
		
		this.hourSpinner = new JSpinner(new SpinnerNumberModel(0,0,99,1));
		this.minuteSpinner = new JSpinner(new SpinnerNumberModel(0,0,59,1));
		this.secondSpinner = new JSpinner(new SpinnerNumberModel(0,0,59,1));
		
		this.blackTimeLabel = new JLabel();
		this.whiteTimeLabel = new JLabel();
		
		this.timeBtn = new JButton("Set time");
		this.resetBtn = new JButton("Reset");
		this.blackBtn = new JButton("Start");
		this.whiteBtn = new JButton("Start");
		
		timeBtn.addActionListener(e -> setTime());
		resetBtn.addActionListener(e -> controller.resetTimers());
		blackBtn.addActionListener(e -> controller.startBlackTimer());
		whiteBtn.addActionListener(e -> controller.startWhiteTimer());
		
		addComponent(window, hourLabel, 0, 0);
		addComponent(window, minLabel, 1, 0);
		addComponent(window, secLabel, 2, 0);
		addComponent(window, hourSpinner, 0, 1);
		addComponent(window, minuteSpinner, 1, 1);
		addComponent(window, secondSpinner, 2, 1);
		addComponent(window, timeBtn, 0, 2, 1, 3);
		
		addComponent(window, blackTimeLabel, 0, 3);
		addComponent(window, whiteTimeLabel, 2, 3);
		addComponent(window, blackBtn, 0, 4);
		addComponent(window, whiteBtn, 2, 4);
		addComponent(window, resetBtn, 0, 5, 1, 3);
	}
	
	private void addComponent(Container container, Component component, int x, int y) {
		addComponent(container, component, x, y, 1, 1);
	}
	
	private void addComponent(Container container, Component component, int x, int y, int height, int width) {
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = x;
		c.gridy = y;
		c.gridheight = height;
		c.gridwidth = width;
		
		container.add(component, c);
	}
	
	private void setTime() {
		int hours = (int) hourSpinner.getValue();
		int mins = (int) minuteSpinner.getValue();
		int secs = (int) secondSpinner.getValue();
		
		controller.setTime(hours, mins, secs);
	}
	
	private void updateGUI() {
		blackTimeLabel.setText(controller.getBlackTimeString());
		whiteTimeLabel.setText(controller.getWhiteTimeString());
		
		blackBtn.setEnabled(controller.isBlackStartable());
		whiteBtn.setEnabled(controller.isWhiteStartable());
	}
	
}
