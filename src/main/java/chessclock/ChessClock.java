package chessclock;

import exceptions.ClockException;
import observer.Observer;
import observer.Subject;

/**
 * Consists of two ClockTimers that each belongs to a Player instance.
 * When one player starts the game their ClockTimer starts ticking.
 * After a game is started the ClockTimers can be swapped between til
 * one player is out of time.
 *
 * Updates of the clock can be subscribed to by sending a callback to
 * the addObserver-method.
 *
 * @author Max Jonsson
 * @version 2020-01-20
 */
public class ChessClock implements Subject {
	public enum Player {
		WHITE(new ClockTimer()),
		BLACK(new ClockTimer());
		
		private final ClockTimer timer;

		private Player(ClockTimer timer) {
			this.timer = timer;
		}
	}
	
	private Observer observer;
	
	public ChessClock() {
		Player.BLACK.timer.addObserver(() -> notifyObservers());
		Player.WHITE.timer.addObserver(() -> notifyObservers());
	}
	
	public final void setInitialTime(int seconds) {
		Player.BLACK.timer.setTime(seconds);
		Player.WHITE.timer.setTime(seconds);
		resetTimers();
	}
	
	/**
	 * @param player The player whose clock should be started.
	 * Use one of the available player instances; ChessClock.Player.BLACK
	 * or ChessClock.Player.WHITE
	 */
	public void startTimer(Player player) {
		if (isStarted())
			throw new ClockException("Clock is already started!");
		
		player.timer.start();
		notifyObservers();
	}
	
	public void resetTimers() {
		Player.BLACK.timer.reset();
		Player.WHITE.timer.reset();
		notifyObservers();
	}
	
	public void swapTimer() {
		if (!isStarted())
			throw new ClockException("Clock is not started!");
		
		swap(Player.BLACK.timer);
		swap(Player.WHITE.timer);
		notifyObservers();
	}
	
	private void swap(ClockTimer timer) {
		if (timer.isRunning())
			timer.pause();
		else
			timer.start();
	}
	
	public int getBlackTime() {
		return Player.BLACK.timer.getTime();
	}
	
	public int getWhiteTime() {
		return Player.WHITE.timer.getTime();
	}
	
	public boolean isBlackRunning() {
		return Player.BLACK.timer.isRunning();
	}
	
	public boolean isWhiteRunning() {
		return Player.WHITE.timer.isRunning();
	}
	
	public boolean isStarted() {
		return isBlackRunning() || isWhiteRunning();
	}
	
	public boolean isTimeUp() {
		return getBlackTime() <= 0 || getWhiteTime() <= 0;
	}
	
	@Override
	public void addObserver(Observer observer) {
		this.observer = observer;
		notifyObservers();
	}
	
	@Override
	public void notifyObservers() {
		if (observer != null)
			observer.update();
	}
	
}
