package chessclock;

import exceptions.TimerException;
import javax.swing.Timer;
import observer.Observer;
import observer.Subject;

/**
 * Timer module that uses javax.swing.Timer to update the current
 * time. The time starts from the initial time sent in, going down
 * to zero. The time updates every second after the timer is started.
 * 
 * The updates can be subscribed to by sending a callback to the
 * addObserver-method.
 *
 * @author Max Jonsson
 * @version 2020-01-20
 */
public class ClockTimer implements Subject {
	private final Timer timer;
	private Observer observer;
	
	private int totalTime;
	private int secondsLeft;
	
	public ClockTimer() {
		this.totalTime = 0;
		this.secondsLeft = 0;
		this.timer = new Timer(1000, e -> updateTime());
	}
	
	public void setTime(int seconds) {
		if (seconds < 0)
			throw new IllegalArgumentException("Time must be positive");
		
		this.totalTime = seconds;
	}

	private void updateTime() {
		if (--secondsLeft <= 0)
			pause();	
		
		notifyObservers();
	}
	
	public void start() {
		if (secondsLeft <= 0)
			throw new TimerException("Time is up!");
		
		timer.start();
	}
	
	public void pause() {
		timer.stop();
	}
	
	public void reset() {
		pause();
		this.secondsLeft = totalTime;
	}
	
	public int getTime() {
		return this.secondsLeft;
	}
	
	public boolean isRunning() {
		return timer.isRunning();
	}
	
	@Override
	public void addObserver(Observer observer) {
		this.observer = observer;
	}

	@Override
	public void notifyObservers() {
		if (observer != null)
			observer.update();
	}
	
}
