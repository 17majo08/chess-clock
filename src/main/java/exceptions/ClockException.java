package exceptions;

/**
 * Exception thrown by the ChessClock.
 * 
 * @author Max Jonsson
 * @version 2020-01-22
 */
public class ClockException extends RuntimeException {
	private static final long serialVersionUID = 1L;
    
	public ClockException(String msg) {
		super(msg);
	}
	
}
