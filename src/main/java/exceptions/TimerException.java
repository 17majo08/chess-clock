package exceptions;

/**
 * Exception thrown by the ClockTimer.
 * 
 * @author Max Jonsson
 * @version 2020-01-22
 */
public class TimerException extends RuntimeException {
	private static final long serialVersionUID = 1L;
    
	public TimerException(String msg) {
		super(msg);
	}
	
}
